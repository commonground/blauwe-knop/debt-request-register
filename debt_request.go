// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package debtrequestregister

type DebtRequest struct {
	Id            string
	BSN           string
	Organizations []Organization
}
