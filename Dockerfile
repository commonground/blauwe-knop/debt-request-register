# Use go 1.x based on alpine image.
FROM golang:1.19.4-alpine AS build

ADD . /go/src/debt-request-register/
ENV GO111MODULE on
WORKDIR /go/src/debt-request-register
RUN go mod download
RUN go build -o dist/bin/debt-request-register ./cmd/debt-request-register

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/debt-request-register/dist/bin/debt-request-register /usr/local/bin/debt-request-register

# Add non-priveleged user
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/debt-request-register"]
