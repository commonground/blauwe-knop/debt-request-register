// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package http

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"debtrequestregister"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

type debtRequestRequestBody struct {
	BSN           string `json:"bsn"`
	Organizations []struct {
		OIN        string `json:"oin"`
		Name       string `json:"name"`
		APIBaseURL string `json:"apiBaseUrl"`
		LoginURL   string `json:"loginUrl"`
	} `json:"organizations"`
}

func (a *debtRequestRequestBody) Bind(r *http.Request) error {
	if len(a.Organizations) < 1 {
		return errors.New("missing at least one organization")
	}

	return nil
}

type debtRequestOrganizationResponseBody struct {
	OIN        string `json:"oin"`
	Name       string `json:"name"`
	APIBaseURL string `json:"apiBaseUrl"`
	LoginURL   string `json:"loginUrl"`
}

type debtRequestResponseBody struct {
	Id            string                                `json:"id"`
	BSN           string                                `json:"bsn"`
	Organizations []debtRequestOrganizationResponseBody `json:"organizations"`
}

func handlerCreate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	debtRequestUseCase, _ := ctx.Value(debtRequestUseCaseKey).(*debtrequestregister.DebtRequestUseCase)

	data := &debtRequestRequestBody{}
	err := render.Bind(r, data)
	if err != nil {
		log.Printf("invalid debt request: %v", err)
		http.Error(w, fmt.Sprintf("invalid debt request: %v", err), http.StatusBadRequest)
		return
	}

	debtRequest := mapDebtRequestRequestBodyToModel(*data)
	debtRequest, err = debtRequestUseCase.Create(debtRequest)
	if err != nil {
		log.Printf("could not create debt request: %v", err)
		http.Error(w, "could not create debt request", http.StatusInternalServerError)
		return
	}

	render.Status(r, http.StatusCreated)
	render.JSON(w, r, mapDebtRequestModelToResponseBody(debtRequest))
}

func handlerGet(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	debtRequestUseCase, _ := ctx.Value(debtRequestUseCaseKey).(*debtrequestregister.DebtRequestUseCase)
	debtRequestID := chi.URLParam(r, "id")

	if debtRequestID == "" {
		log.Printf("missing id")
		http.Error(w, "missing id", http.StatusBadRequest)
		return
	}

	debtRequest, err := debtRequestUseCase.Get(debtRequestID)
	if err != nil {
		log.Printf("could not retrieve debt request: %v", err)
		http.Error(w, "could not retrieve debt request", http.StatusInternalServerError)
		return
	}

	if debtRequest == nil {
		log.Printf("could not find debt request")
		http.Error(w, "could not find debt request", http.StatusNotFound)
		return
	}
	render.JSON(w, r, mapDebtRequestModelToResponseBody(*debtRequest))
}

func handlerUpdate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	debtRequestUseCase, _ := ctx.Value(debtRequestUseCaseKey).(*debtrequestregister.DebtRequestUseCase)

	data := &debtRequestRequestBody{}
	err := render.Bind(r, data)
	if err != nil {
		log.Printf("invalid debt request: %v", err)
		http.Error(w, fmt.Sprintf("invalid debt request: %v", err), http.StatusBadRequest)
		return
	}

	debtRequestID := chi.URLParam(r, "id")
	if debtRequestID == "" {
		log.Printf("missing id")
		http.Error(w, "missing id", http.StatusBadRequest)
		return
	}

	debtRequest, err := debtRequestUseCase.Get(debtRequestID)
	if err != nil {
		log.Printf("could not retrieve debt request: %v", err)
		http.Error(w, "could not retrieve debt request", http.StatusInternalServerError)
		return
	}

	if debtRequest == nil {
		log.Printf("could not find debt request")
		http.Error(w, "could not find debt request", http.StatusNotFound)
		return
	}

	newDebtRequestModel := mapDebtRequestRequestBodyToModel(*data)
	newDebtRequestModel.Id = debtRequest.Id

	err = debtRequestUseCase.Update(debtRequest.Id, newDebtRequestModel)
	if err != nil {
		log.Printf("could not update debt request: %v", err)
		http.Error(w, "could not update debt request", http.StatusInternalServerError)
		return
	}

	render.Status(r, http.StatusOK)
	render.JSON(w, r, mapDebtRequestModelToResponseBody(newDebtRequestModel))
}

func handlerDelete(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	debtRequestUseCase, _ := ctx.Value(debtRequestUseCaseKey).(*debtrequestregister.DebtRequestUseCase)

	debtRequestID := chi.URLParam(r, "id")
	if debtRequestID == "" {
		log.Printf("missing id")
		http.Error(w, "missing id", http.StatusBadRequest)
		return
	}

	debtRequest, err := debtRequestUseCase.Get(debtRequestID)
	if err != nil {
		log.Printf("could not retrieve debt request: %v", err)
		http.Error(w, "could not retrieve debt request", http.StatusInternalServerError)
		return
	}

	if debtRequest == nil {
		log.Printf("could not find debt request")
		http.Error(w, "could not find debt request", http.StatusNotFound)
		return
	}

	err = debtRequestUseCase.Delete(debtRequest.Id)
	if err != nil {
		log.Printf("could not delete debt request: %v", err)
		http.Error(w, "could not delete debt request", http.StatusInternalServerError)
		return
	}
	render.Status(r, http.StatusOK)

}

func mapDebtRequestRequestBodyToModel(body debtRequestRequestBody) debtrequestregister.DebtRequest {
	var result debtrequestregister.DebtRequest

	result.BSN = body.BSN

	for _, organization := range body.Organizations {
		result.Organizations = append(result.Organizations, debtrequestregister.Organization{
			OIN:        organization.OIN,
			Name:       organization.Name,
			APIBaseURL: organization.APIBaseURL,
			LoginURL:   organization.LoginURL,
		})
	}

	return result
}

func mapDebtRequestModelToResponseBody(request debtrequestregister.DebtRequest) debtRequestResponseBody {
	var result debtRequestResponseBody

	result.Id = request.Id
	result.BSN = request.BSN

	for _, organization := range request.Organizations {
		result.Organizations = append(result.Organizations, debtRequestOrganizationResponseBody{
			OIN:        organization.OIN,
			Name:       organization.Name,
			APIBaseURL: organization.APIBaseURL,
			LoginURL:   organization.LoginURL,
		})
	}

	return result
}
