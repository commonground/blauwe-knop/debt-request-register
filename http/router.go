// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"context"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"

	"debtrequestregister"
)

func NewRouter(apiKey string, debtRequestUseCase *debtrequestregister.DebtRequestUseCase) *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	if apiKey != "" {
		r.Use(authenticatedOnly(apiKey))
	}

	r.Route("/debt-requests", func(r chi.Router) {
		r.Post("/",
			func(w http.ResponseWriter, r *http.Request) {
				ctx := context.WithValue(r.Context(), debtRequestUseCaseKey, debtRequestUseCase)
				handlerCreate(w, r.WithContext(ctx))
			})
		r.Get("/{id}",
			func(w http.ResponseWriter, r *http.Request) {
				ctx := context.WithValue(r.Context(), debtRequestUseCaseKey, debtRequestUseCase)
				handlerGet(w, r.WithContext(ctx))
			})
		r.Put("/{id}",
			func(w http.ResponseWriter, r *http.Request) {
				ctx := context.WithValue(r.Context(), debtRequestUseCaseKey, debtRequestUseCase)
				handlerUpdate(w, r.WithContext(ctx))
			})
		r.Delete("/{id}",
			func(w http.ResponseWriter, r *http.Request) {
				ctx := context.WithValue(r.Context(), debtRequestUseCaseKey, debtRequestUseCase)
				handlerDelete(w, r.WithContext(ctx))
			})
	})

	healthCheckHandler := healthcheck.NewHandler("debt-request-register", debtRequestUseCase.GetHealthChecks())
	r.Route("/health", func(r chi.Router) {
		r.Get("/", healthCheckHandler.HandleHealth)
		r.Get("/check", healthCheckHandler.HandlerHealthCheck)
	})

	return r
}

func authenticatedOnly(apiKey string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			providedApiKey := r.Header.Get("Authentication")

			if providedApiKey != apiKey {
				log.Printf("Unauthorized: apikey unvalid")
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}
			next.ServeHTTP(w, r)
		})
	}
}

type key int

const (
	debtRequestUseCaseKey key = iota
)
