package http_test

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"

	"debtrequestregister"
	http_infra "debtrequestregister/http"
	"debtrequestregister/mock"
)

const DummyDebtRequestID = "dummy-debt-request-id"
const MockId = "mock-id"

var DummyDebtRequest = debtrequestregister.DebtRequest{
	Id:  DummyDebtRequestID,
	BSN: "000000000",
	Organizations: []debtrequestregister.Organization{
		{
			Name:       "dummy-organization",
			OIN:        "00000000000000000000",
			APIBaseURL: "https://api-base-url.nl",
			LoginURL:   "https://login-url.nl",
		},
	},
}

func generateDebtRequestRepository(t *testing.T) *mock.MockDebtRequestRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockDebtRequestRepository(ctrl)
	return repo
}

func generateTokenGenerator(t *testing.T) *mock.MockTokenGenerator {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockTokenGenerator(ctrl)
	return repo
}

func Test_CreateRouter_Authentication(t *testing.T) {
	logger := zap.NewNop()

	type fields struct {
		apiKey                string
		debtRequestRepository debtrequestregister.DebtRequestRepository
		tokenGenerator        debtrequestregister.TokenGenerator
	}
	type args struct {
		apiKey        string
		debtRequestID string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
	}{
		{
			"authentication disabled",
			fields{
				debtRequestRepository: func() debtrequestregister.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Get(DummyDebtRequestID).Return(&DummyDebtRequest, nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateTokenGenerator(t),
				apiKey:         "",
			},
			args{
				apiKey:        "",
				debtRequestID: DummyDebtRequestID,
			},
			http.StatusOK,
		},
		{
			"authentication enabled - with incorrect key provided",
			fields{
				debtRequestRepository: generateDebtRequestRepository(t),
				tokenGenerator:        generateTokenGenerator(t),
				apiKey:                "dummy-api-key",
			},
			args{
				apiKey:        "incorrect-key",
				debtRequestID: DummyDebtRequestID,
			},
			http.StatusUnauthorized,
		},
		{
			"authentication enabled - with correct key provided",
			fields{
				debtRequestRepository: func() debtrequestregister.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Get(DummyDebtRequestID).Return(&DummyDebtRequest, nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateTokenGenerator(t),
				apiKey:         "dummy-api-key",
			},
			args{
				apiKey:        "dummy-api-key",
				debtRequestID: DummyDebtRequestID,
			},
			http.StatusOK,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter(test.fields.apiKey, debtrequestregister.NewDebtRequestUseCase(logger, test.fields.debtRequestRepository, test.fields.tokenGenerator))
			w := httptest.NewRecorder()

			url := fmt.Sprintf("/debt-requests/%v", test.args.debtRequestID)
			request := httptest.NewRequest(http.MethodGet, url, nil)

			if test.args.apiKey != "" {
				request.Header.Set("Authentication", test.args.apiKey)
			}

			router.ServeHTTP(w, request)
			resp := w.Result()

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
		})
	}
}

func Test_CreateRouter_CreateDebtRequest(t *testing.T) {
	logger := zap.NewNop()

	type fields struct {
		debtRequestRepository debtrequestregister.DebtRequestRepository
		tokenGenerator        debtrequestregister.TokenGenerator
	}
	type args struct {
		requestBody io.Reader
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"without organization",
			fields{
				debtRequestRepository: func() debtrequestregister.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Create(gomock.Any()).Return(nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: func() debtrequestregister.TokenGenerator {
					generator := generateTokenGenerator(t)
					generator.EXPECT().GenerateIdToken().Return(MockId).AnyTimes()
					return generator
				}(),
			},
			args{
				requestBody: func() io.Reader {
					body := "{}\n"
					bodyBytes := []byte(body)
					return bytes.NewReader(bodyBytes)
				}(),
			},
			http.StatusBadRequest,
			"invalid debt request: missing at least one organization\n",
		},
		{
			"happy flow",
			fields{
				debtRequestRepository: func() debtrequestregister.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Create(gomock.Any()).Return(nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: func() debtrequestregister.TokenGenerator {
					generator := generateTokenGenerator(t)
					generator.EXPECT().GenerateIdToken().Return(MockId).AnyTimes()
					return generator
				}(),
			},
			args{
				requestBody: func() io.Reader {
					body := "{\"organizations\": [{\"oin\": \"000000000\"}]}\n"
					bodyBytes := []byte(body)
					return bytes.NewReader(bodyBytes)
				}(),
			},
			http.StatusCreated,
			"{\"id\":\"mock-id\",\"bsn\":\"\",\"organizations\":[{\"oin\":\"000000000\",\"name\":\"\",\"apiBaseUrl\":\"\",\"loginUrl\":\"\"}]}\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter("", debtrequestregister.NewDebtRequestUseCase(logger, test.fields.debtRequestRepository, test.fields.tokenGenerator))
			w := httptest.NewRecorder()

			request := httptest.NewRequest(http.MethodPost, "/debt-requests", test.args.requestBody)
			request.Header.Set("Content-Type", "application/json")

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, err := ioutil.ReadAll(resp.Body)

			assert.Nil(t, err)
			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}

func Test_CreateRouter_GetDebtRequest(t *testing.T) {
	logger := zap.NewNop()

	type fields struct {
		debtRequestRepository debtrequestregister.DebtRequestRepository
		tokenGenerator        debtrequestregister.TokenGenerator
	}
	type args struct {
		debtRequestID string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"when the debt request does not exist",
			fields{
				debtRequestRepository: func() debtrequestregister.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Get(DummyDebtRequestID).Return(nil, nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateTokenGenerator(t),
			},
			args{
				debtRequestID: DummyDebtRequestID,
			},
			http.StatusNotFound,
			"could not find debt request\n",
		},
		{
			"when the database returns an error",
			fields{
				debtRequestRepository: func() debtrequestregister.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Get(DummyDebtRequestID).Return(nil, errors.New("arbitrary error")).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateTokenGenerator(t),
			},
			args{
				debtRequestID: DummyDebtRequestID,
			},
			http.StatusInternalServerError,
			"could not retrieve debt request\n",
		},
		{
			"happy flow",
			fields{
				debtRequestRepository: func() debtrequestregister.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Get(DummyDebtRequestID).Return(&DummyDebtRequest, nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateTokenGenerator(t),
			},
			args{
				debtRequestID: DummyDebtRequestID,
			},
			http.StatusOK,
			"{\"id\":\"dummy-debt-request-id\",\"bsn\":\"000000000\",\"organizations\":[{\"oin\":\"00000000000000000000\",\"name\":\"dummy-organization\",\"apiBaseUrl\":\"https://api-base-url.nl\",\"loginUrl\":\"https://login-url.nl\"}]}\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter("", debtrequestregister.NewDebtRequestUseCase(logger, test.fields.debtRequestRepository, test.fields.tokenGenerator))
			w := httptest.NewRecorder()

			url := fmt.Sprintf("/debt-requests/%s", test.args.debtRequestID)
			request := httptest.NewRequest(http.MethodGet, url, nil)

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, err := ioutil.ReadAll(resp.Body)

			assert.Nil(t, err)
			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}

func Test_CreateRouter_UpdateDebtRequest(t *testing.T) {
	logger := zap.NewNop()

	type fields struct {
		debtRequestRepository debtrequestregister.DebtRequestRepository
		tokenGenerator        debtrequestregister.TokenGenerator
	}
	type args struct {
		debtRequestId string
		requestBody   io.Reader
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"without an organization",
			fields{
				debtRequestRepository: func() debtrequestregister.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Get(DummyDebtRequestID).Return(&DummyDebtRequest, nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateTokenGenerator(t),
			},
			args{
				requestBody: func() io.Reader {
					body := "{}\n"
					bodyBytes := []byte(body)
					return bytes.NewReader(bodyBytes)
				}(),
				debtRequestId: DummyDebtRequestID,
			},
			http.StatusBadRequest,
			"invalid debt request: missing at least one organization\n",
		},
		{
			"happy flow",
			fields{
				debtRequestRepository: func() debtrequestregister.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Get(DummyDebtRequestID).Return(&DummyDebtRequest, nil).AnyTimes()
					repo.EXPECT().Update(DummyDebtRequestID, debtrequestregister.DebtRequest{
						Id:  DummyDebtRequestID,
						BSN: "000000000",
						Organizations: []debtrequestregister.Organization{
							{
								OIN:        "000000001",
								Name:       "dummy-organization-name",
								APIBaseURL: "https://api-base-url.nl",
								LoginURL:   "https://login-url.nl",
							},
						},
					}).Return(nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateTokenGenerator(t),
			},
			args{
				requestBody: func() io.Reader {
					body := "{\"bsn\": \"000000000\",\"organizations\": [{\"oin\": \"000000001\", \"name\": \"dummy-organization-name\", \"apiBaseUrl\": \"https://api-base-url.nl\", \"loginUrl\": \"https://login-url.nl\"}]}\n"
					bodyBytes := []byte(body)
					return bytes.NewReader(bodyBytes)
				}(),
				debtRequestId: DummyDebtRequestID,
			},
			http.StatusOK,
			"{\"id\":\"dummy-debt-request-id\",\"bsn\":\"000000000\",\"organizations\":[{\"oin\":\"000000001\",\"name\":\"dummy-organization-name\",\"apiBaseUrl\":\"https://api-base-url.nl\",\"loginUrl\":\"https://login-url.nl\"}]}\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter("", debtrequestregister.NewDebtRequestUseCase(logger, test.fields.debtRequestRepository, test.fields.tokenGenerator))
			w := httptest.NewRecorder()

			url := fmt.Sprintf("/debt-requests/%s", test.args.debtRequestId)
			request := httptest.NewRequest(http.MethodPut, url, test.args.requestBody)
			request.Header.Set("Content-Type", "application/json")

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, err := ioutil.ReadAll(resp.Body)

			assert.Nil(t, err)
			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}

func Test_CreateRouter_DeleteDebtRequest(t *testing.T) {
	logger := zap.NewNop()

	type fields struct {
		debtRequestRepository debtrequestregister.DebtRequestRepository
		tokenGenerator        debtrequestregister.TokenGenerator
	}
	type args struct {
		debtRequestID string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"happy flow",
			fields{
				debtRequestRepository: func() debtrequestregister.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Get(DummyDebtRequestID).Return(&DummyDebtRequest, nil).AnyTimes()
					repo.EXPECT().Delete(DummyDebtRequestID).Return(nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateTokenGenerator(t),
			},
			args{
				debtRequestID: DummyDebtRequestID,
			},
			http.StatusOK,
			"",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter("", debtrequestregister.NewDebtRequestUseCase(logger, test.fields.debtRequestRepository, test.fields.tokenGenerator))
			w := httptest.NewRecorder()

			url := fmt.Sprintf("/debt-requests/%s", test.args.debtRequestID)
			request := httptest.NewRequest(http.MethodDelete, url, nil)

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, err := ioutil.ReadAll(resp.Body)

			assert.Nil(t, err)
			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}
