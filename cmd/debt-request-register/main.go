// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"
	"net/http"

	"go.uber.org/zap"

	"debtrequestregister"
	http_infra "debtrequestregister/http"
	"debtrequestregister/redis"
)

type options struct {
	ListenAddress string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:8086" description:"Address for the debt-request-register api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	RedisDSN      string `long:"redis-dsn" env:"REDIS_DSN" default:"redis://0.0.0.0:6379/0" description:"DSN for the redis database. Read https://pkg.go.dev/github.com/go-redis/redis/v8?tab=doc#ParseURL for more info"`
	APIKey        string `long:"api-key" env:"API_KEY" default:"" description:"Key to protect the API endpoints."`
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	tokenGenerator := debtrequestregister.NewRandomTokenGenerator()
	debtRequestRepository, err := redis.NewSchuldenRequestRepository(cliOptions.RedisDSN)
	if err != nil {
		log.Fatalf("failed to initialize redis debt request repository: %v", err)
	}

	debtRequestUseCase := debtrequestregister.NewDebtRequestUseCase(logger, debtRequestRepository, tokenGenerator)
	router := http_infra.NewRouter(cliOptions.APIKey, debtRequestUseCase)

	logger.Info(fmt.Sprintf("start listening on %s", cliOptions.ListenAddress))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		if err != http.ErrServerClosed {
			panic(err)
		}
	}
}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
