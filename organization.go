// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package debtrequestregister

type Organization struct {
	OIN        string
	Name       string
	APIBaseURL string
	LoginURL   string
}
