package redis

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"time"

	"github.com/go-redis/redis/v7"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"

	"debtrequestregister"
)

type organizationModel struct {
	OIN        string `json:"oin"`
	Name       string `json:"name"`
	APIBaseURL string `json:"apiBaseUrl"`
	LoginURL   string `json:"loginUrl"`
}

type saveRequestModel struct {
	Id            string `json:"Id"`
	BSN           string `json:"bsn"`
	Organizations []*organizationModel
}

type requestResponseModel struct {
	Id            string `json:"Id"`
	BSN           string `json:"bsn"`
	Organizations []*organizationModel
}

type SchuldenRequestRepository struct {
	client *redis.Client
}

const debtRequestKeyPrefix = "schulden-request"

func NewSchuldenRequestRepository(redisDSN string) (*SchuldenRequestRepository, error) {
	options, err := redis.ParseURL(redisDSN)
	if err != nil {
		return nil, err
	}

	client := redis.NewClient(options)

	_, err = client.Ping().Result()
	if err != nil {
		return nil, fmt.Errorf("ping to redis failed: %v", err)
	}
	rand.Seed(time.Now().UnixNano())
	return &SchuldenRequestRepository{
		client: client,
	}, nil
}

func (r *SchuldenRequestRepository) Create(request debtrequestregister.DebtRequest) error {
	saveRequest := mapSchuldenRequestToRedisSaveRequestModel(request)

	requestBytes, err := json.Marshal(saveRequest)
	if err != nil {
		return err
	}

	_, err = r.client.Set(fmt.Sprintf("%s-%s", debtRequestKeyPrefix, request.Id), string(requestBytes), 0).Result()
	return err
}

func mapSchuldenRequestToRedisSaveRequestModel(request debtrequestregister.DebtRequest) saveRequestModel {
	var result saveRequestModel

	result.Id = request.Id
	result.BSN = request.BSN

	for _, organization := range request.Organizations {
		result.Organizations = append(result.Organizations, &organizationModel{
			organization.OIN,
			organization.Name,
			organization.APIBaseURL,
			organization.LoginURL,
		})
	}

	return result
}

func (r *SchuldenRequestRepository) Update(id string, request debtrequestregister.DebtRequest) error {
	updateRequest := mapSchuldenRequestToRedisSaveRequestModel(request)

	requestBytes, err := json.Marshal(updateRequest)
	if err != nil {
		return err
	}

	_, err = r.client.Set(fmt.Sprintf("%s-%s", debtRequestKeyPrefix, id), string(requestBytes), 0).Result()
	return err
}

func (r *SchuldenRequestRepository) Get(id string) (*debtrequestregister.DebtRequest, error) {
	value, err := r.client.Get(fmt.Sprintf("%s-%s", debtRequestKeyPrefix, id)).Result()
	if err != nil {
		if err == redis.Nil {
			return nil, nil
		}

		return nil, err
	}

	responseModel := &requestResponseModel{}
	err = json.Unmarshal([]byte(value), responseModel)
	if err != nil {
		return nil, err
	}

	return mapRedisGetRequestModelToRequestModel(responseModel), nil
}

func mapRedisGetRequestModelToRequestModel(requestResponse *requestResponseModel) *debtrequestregister.DebtRequest {
	var result debtrequestregister.DebtRequest

	result.Id = requestResponse.Id
	result.BSN = requestResponse.BSN

	for _, organization := range requestResponse.Organizations {
		result.Organizations = append(result.Organizations, debtrequestregister.Organization{
			OIN:        organization.OIN,
			Name:       organization.Name,
			APIBaseURL: organization.APIBaseURL,
			LoginURL:   organization.LoginURL,
		})
	}

	return &result
}

func (r *SchuldenRequestRepository) Delete(id string) error {
	_, err := r.client.Del(fmt.Sprintf("%s-%s", debtRequestKeyPrefix, id)).Result()
	return err
}

func (s *SchuldenRequestRepository) GetHealthCheck() healthcheck.Result {
	name := "redis"
	status := healthcheck.StatusOK
	start := time.Now()

	pong, err := s.client.Ping().Result()
	if err != nil {
		log.Printf("ping to redis failed: %v", err)
		status = healthcheck.StatusError
	} else {
		log.Printf("ping to redis successful: %s", pong)
	}

	return healthcheck.Result{
		Name:         name,
		Status:       status,
		ResponseTime: time.Since(start).Seconds(),
		HealthChecks: nil,
	}
}
