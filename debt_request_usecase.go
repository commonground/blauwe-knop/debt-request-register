package debtrequestregister

import (
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
	"go.uber.org/zap"
)

type DebtRequestRepository interface {
	Get(id string) (*DebtRequest, error)
	Create(debtRequest DebtRequest) error
	Update(id string, debtRequest DebtRequest) error
	Delete(id string) error
	healthcheck.Checker
}

type TokenGenerator interface {
	GenerateIdToken() string
}

type DebtRequestUseCase struct {
	debtRequestRepository DebtRequestRepository
	tokenGenerator        TokenGenerator
	logger                *zap.Logger
}

func NewDebtRequestUseCase(logger *zap.Logger, debtRequestRepository DebtRequestRepository, tokenGenerator TokenGenerator) *DebtRequestUseCase {
	return &DebtRequestUseCase{
		debtRequestRepository: debtRequestRepository,
		tokenGenerator:        tokenGenerator,
		logger:                logger,
	}
}

func (rs *DebtRequestUseCase) Create(debtRequest DebtRequest) (DebtRequest, error) {
	debtRequest.Id = rs.tokenGenerator.GenerateIdToken()
	err := rs.debtRequestRepository.Create(debtRequest)
	return debtRequest, err
}

func (rs *DebtRequestUseCase) Get(debtRequestID string) (*DebtRequest, error) {
	return rs.debtRequestRepository.Get(debtRequestID)
}

func (rs DebtRequestUseCase) Update(debtRequestID string, debtRequest DebtRequest) error {
	return rs.debtRequestRepository.Update(debtRequest.Id, debtRequest)
}

func (rs *DebtRequestUseCase) Delete(debtRequestID string) error {
	return rs.debtRequestRepository.Delete(debtRequestID)
}

func (a *DebtRequestUseCase) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{
		a.debtRequestRepository,
	}
}
